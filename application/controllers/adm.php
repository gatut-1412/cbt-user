<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set("Asia/Jakarta");

class Adm extends CI_Controller {
	
	function __construct() {
	        parent::__construct();
	        $this->db->query("SET time_zone='+7:00'");
	}
    
	public function cek_aktif() {
		if ($this->session->userdata('admin_valid') == false && $this->session->userdata('admin_id') == "") {
			redirect('adm/login');
		} 
	}
	
	public function index() {
		$this->cek_aktif();
		
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		
		$a['p']			= "v_main";

		if ($a['sess_level'] == "SISWA") {
			$a['p_mapel']	= $this->db->query("SELECT m_mapel.nama FROM tr_siswa_mapel INNER JOIN m_mapel ON tr_siswa_mapel.id_mapel = m_mapel.id WHERE tr_siswa_mapel.id_siswa = '".$a['sess_konid']."'")->result();
		}
		
		$this->load->view('aaa', $a);
	}
	
	/* == SISWA == */
	public function ikuti_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);

		//var post from json
		$p = json_decode(file_get_contents('php://input'));

		//return as json
		$jeson = array();

		$a['data'] = $this->db->query("SELECT tb_mapping_cbt.*,tb_master_kelas.nama_kelas as nama_kelas, tb_guru_mapel.*,tb_kelas_siswa.id_siswa	,tb_soal_cbt.judul_soal as nama_ujian
										,tb_pelajaran.nama_pelajaran as mapel ,  tb_soal_cbt.waktu as waktu,tb_soal_cbt.jenis_soal as jenis_soal,(SELECT COUNT(id_detail_soal_cbt) FROM tb_detail_soal_cbt WHERE tb_soal_cbt.id_soal_cbt = tb_detail_soal_cbt.id_soal_cbt ) AS jumlah_soal,
											(SELECT COUNT(id) FROM tr_ikut_ujian WHERE tr_ikut_ujian.id_user = ".$a['sess_konid']." AND tr_ikut_ujian.id_tes = 
											tb_mapping_cbt.id_mapping_cbt) AS sudah_ikut
											FROM tb_mapping_cbt
										 JOIN tb_guru_mapel ON tb_mapping_cbt.id_guru_mapel = tb_guru_mapel.id_guru_mapel
										 JOIN tb_master_kelas ON tb_master_kelas.id_master_kelas = tb_guru_mapel.id_master_kelas
										 JOIN tb_guru ON tb_guru.id_guru = tb_guru_mapel.id_guru
										 JOIN tb_pelajaran ON tb_pelajaran.id_pelajaran = tb_guru_mapel.id_pelajaran
										 JOIN tb_soal_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
										 join tb_detail_soal_cbt on tb_detail_soal_cbt.id_soal_cbt = tb_soal_cbt.id_soal_cbt
										 JOIN tb_kelas_siswa on tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas
										 WHERE tb_kelas_siswa.id_siswa = ".$a['sess_konid']."
										 AND tb_mapping_cbt.status_mapping in('PUBLISH','SELESAI')
										 group by id_mapping_cbt
										ORDER BY tb_mapping_cbt.id_mapping_cbt DESC  limit 10")->result();
		//echo $this->db->last_query();
		$a['p']	= "m_ikut_ujian";
		// var_dump($a['data']);
		// exit();
		$this->load->view('aaa', $a);
	}

	public function ikut_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
			

		//var post from json
		$p = json_decode(file_get_contents('php://input'));

		$a['detil_user'] = $this->db->query("SELECT * FROM tb_siswa WHERE id_siswa = '".$a['sess_konid']."'")->row();


		if ($uri3 == "simpan_satu") {
			$p			= json_decode(file_get_contents('php://input'));
			
			$update_ 	= "";
			for ($i = 1; $i < $p->jml_soal; $i++) {
				$_tjawab 	= "opsi_".$i;
				$_tidsoal 	= "id_soal_".$i;

				$jawaban_ 	= empty($p->$_tjawab) ? "" : $p->$_tjawab;

				$update_	.= "".$p->$_tidsoal.":".$jawaban_.",";
			}
			$update_		= substr($update_, 0, -1);


			$this->db->query("UPDATE tr_ikut_ujian SET list_jawaban = '".$update_."' WHERE id_tes = '$uri4' AND id_user = '".$a['sess_konid']."'");
			echo $this->db->last_query();
			exit;			
		} else if ($uri3 == "simpan_akhir") {
			$p			= json_decode(file_get_contents('php://input'));
			
			$jumlah_soal = $p->jml_soal;
			$jumlah_benar = 0;
			$jumlah_bobot = 0;
			$update_ = "";

			for ($i = 1; $i < $p->jml_soal; $i++) {
				$_tjawab 	= "opsi_".$i;
				$_tidsoal 	= "id_soal_".$i;

				$jawaban_ 	= empty($p->$_tjawab) ? "" : $p->$_tjawab;

				$cek_jwb 	= $this->db->query("SELECT bobot, kunci_jawaban as jawaban FROM tb_detail_soal_cbt WHERE id_detail_soal_cbt = '".$p->$_tidsoal."'")->row();
				if ($cek_jwb->jawaban == $jawaban_) {
					$jumlah_benar++;
					$jumlah_bobot = $jumlah_bobot + $cek_jwb->bobot;
				}
				$update_	.= "".$p->$_tidsoal.":".$jawaban_.",";
			}
			$update_		= substr($update_, 0, -1);

			$waktuselesai = date("Y-m-d H:i:s");

			$nilai = $jumlah_bobot; //($jumlah_benar/($jumlah_soal-1)) * 100;
			$this->db->query("UPDATE tr_ikut_ujian SET tgl_selesai = NOW(), jml_benar = ".$jumlah_benar.", nilai_bobot = ".$jumlah_bobot.", nilai = '".$nilai."', list_jawaban = '".$update_."', status = 'N' WHERE id_tes = '$uri4' AND id_user = '".$a['sess_konid']."'");
			$a['status'] = "ok";
			$this->j($a);
			exit;		

		} else {

			$cek_sdh_selesai= $this->db->query("SELECT id FROM tr_ikut_ujian WHERE id_tes = '$uri4' AND id_user = '".$a['sess_konid']."' AND status = 'N'")->num_rows();
			//sekalian validasi waktu sudah berlalu...

			if ($cek_sdh_selesai < 1) {
				//ambil detil soal
				$cek_detil_tes = $this->db->query("SELECT tb_soal_cbt.*, count(tb_detail_soal_cbt.id_soal_cbt) as jumlah_soal ,tb_soal_cbt.judul_soal as nama_ujian, tb_pelajaran.id_pelajaran as id_mapel FROM tb_mapping_cbt
										 JOIN tb_guru_mapel ON tb_mapping_cbt.id_guru_mapel = tb_guru_mapel.id_guru_mapel
										 JOIN tb_master_kelas ON tb_master_kelas.id_master_kelas = tb_guru_mapel.id_master_kelas
										 JOIN tb_guru ON tb_guru.id_guru = tb_guru_mapel.id_guru
										 JOIN tb_pelajaran ON tb_pelajaran.id_pelajaran = tb_guru_mapel.id_pelajaran
										 JOIN tb_soal_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
										 join tb_detail_soal_cbt on tb_detail_soal_cbt.id_soal_cbt = tb_soal_cbt.id_soal_cbt
										 JOIN tb_kelas_siswa on tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas
										  WHERE tb_mapping_cbt.id_mapping_cbt = '$uri4'
										   AND tb_kelas_siswa.id_siswa = ".$a['sess_konid']."")->row();

				$q_cek_sdh_ujian= $this->db->query("SELECT id FROM tr_ikut_ujian WHERE id_tes = '$uri4' AND id_user = '".$a['sess_konid']."'");
				$d_cek_sdh_ujian= $q_cek_sdh_ujian->row();
				$cek_sdh_ujian	= $q_cek_sdh_ujian->num_rows();

				if ($cek_sdh_ujian <1)	{		
					$soal_urut_ok = array();


					$q_soal	= $this->db->query("SELECT tb_detail_soal_cbt.id_detail_soal_cbt as id,tb_detail_soal_cbt.gambar_soal as gambar, tb_detail_soal_cbt.soal_pg as soal,tb_detail_soal_cbt.pilihan_a as opsi_a,tb_detail_soal_cbt.pilihan_b as opsi_b,tb_detail_soal_cbt.pilihan_c as opsi_c, tb_detail_soal_cbt.pilihan_d as opsi_d,tb_detail_soal_cbt.pilihan_e as opsi_e, '' AS jawaban FROM tb_detail_soal_cbt
						 join tb_soal_cbt on tb_detail_soal_cbt.id_soal_cbt = tb_soal_cbt.id_soal_cbt
						 JOIN tb_mapping_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
						JOIN tb_guru_mapel ON tb_mapping_cbt.id_guru_mapel = tb_guru_mapel.id_guru_mapel
						JOIN tb_pelajaran ON tb_pelajaran.id_pelajaran = tb_guru_mapel.id_pelajaran
                        JOIN tb_master_kelas ON tb_master_kelas.id_master_kelas = tb_guru_mapel.id_master_kelas
                        JOIN tb_kelas_siswa on tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas
					 WHERE tb_pelajaran.id_pelajaran = '".$cek_detil_tes->id_mapel."'
					 and tb_mapping_cbt.id_mapping_cbt = '$uri4'
                      AND tb_kelas_siswa.id_siswa = ".$a['sess_konid']." ORDER BY RAND() LIMIT ".$cek_detil_tes->jumlah_soal)->result();
					$i = 0;
					// var_dump($q_soal);
					// exit();
					foreach ($q_soal as $s) {
						$soal_per = new stdClass();
						$soal_per->id = $s->id;
						$soal_per->soal = $s->soal;
						$soal_per->gambar = $s->gambar;
						$soal_per->opsi_a = $s->opsi_a;
						$soal_per->opsi_b = $s->opsi_b;
						$soal_per->opsi_c = $s->opsi_c;
						$soal_per->opsi_d = $s->opsi_d;
						$soal_per->opsi_e = $s->opsi_e;
						$soal_per->jawaban = $s->jawaban;
						$soal_urut_ok[$i] = $soal_per;
						$i++;
					}
					$soal_urut_ok = $soal_urut_ok;

					$list_id_soal	= "";
					$list_jw_soal 	= "";
					if (!empty($q_soal)) {
						foreach ($q_soal as $d) {
							$list_id_soal .= $d->id.",";
							$list_jw_soal .= $d->id.":,";
						}
					}
					$list_id_soal = substr($list_id_soal, 0, -1);
					$list_jw_soal = substr($list_jw_soal, 0, -1);
					$waktu_selesai = tambah_jam_sql($cek_detil_tes->waktu);

					$this->db->query("INSERT INTO tr_ikut_ujian VALUES (null, '$uri4', '".$a['sess_konid']."', '$list_id_soal', '$list_jw_soal', 0, 0, 0, NOW(), ADDTIME(NOW(), '$waktu_selesai'), 'Y')");
					
					$a['detil_soal'] = $this->db->query("SELECT tb_soal_cbt.*, tb_soal_cbt.judul_soal as nama_ujian,tb_guru.nama_guru as namaGuru, tb_pelajaran.nama_pelajaran as namaMapel, count(tb_detail_soal_cbt.id_soal_cbt) as jumlah_soal , tb_pelajaran.id_pelajaran as id_mapel FROM tb_mapping_cbt
										 JOIN tb_guru_mapel ON tb_mapping_cbt.id_guru_mapel = tb_guru_mapel.id_guru_mapel
										 JOIN tb_master_kelas ON tb_master_kelas.id_master_kelas = tb_guru_mapel.id_master_kelas
										 JOIN tb_guru ON tb_guru.id_guru = tb_guru_mapel.id_guru
										 JOIN tb_pelajaran ON tb_pelajaran.id_pelajaran = tb_guru_mapel.id_pelajaran
										 JOIN tb_soal_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
										 join tb_detail_soal_cbt on tb_detail_soal_cbt.id_soal_cbt = tb_soal_cbt.id_soal_cbt
										 JOIN tb_kelas_siswa on tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas
										  WHERE tb_mapping_cbt.id_mapping_cbt  = '$uri4'")->row();

					$a['detiltes'] = $this->db->query("SELECT tr_ikut_ujian.* ,tb_mapping_cbt.updated_timestamp as sisawaktu FROM tr_ikut_ujian 
					 JOIN tb_mapping_cbt ON tr_ikut_ujian.id_tes = tb_mapping_cbt.id_mapping_cbt
					  JOIN tb_soal_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
					   WHERE id_tes = '$uri4' AND id_user = '".$a['sess_konid']."'")->row();
					$a['data']= $soal_urut_ok;
				} else {
					$q_ambil_soal 	= $this->db->query("SELECT tr_ikut_ujian.* ,tb_mapping_cbt.updated_timestamp as sisawaktu FROM tr_ikut_ujian 
					 JOIN tb_mapping_cbt ON tr_ikut_ujian.id_tes = tb_mapping_cbt.id_mapping_cbt
					  JOIN tb_soal_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
					   WHERE id_tes = '$uri4' AND id_user = '".$a['sess_konid']."'")->row();
					$urut_soal 		= explode(",", $q_ambil_soal->list_jawaban);

					$soal_urut_ok	= array();

					for ($i = 0; $i < sizeof($urut_soal); $i++) {
						$pc_urut_soal = explode(":",$urut_soal[$i]);
						$pc_urut_soal1 = empty($pc_urut_soal[1]) ? "''" : "'".$pc_urut_soal[1]."'";

						$ambil_soal = $this->db->query("SELECT tb_detail_soal_cbt.id_detail_soal_cbt as id,tb_detail_soal_cbt.gambar_soal as gambar, tb_detail_soal_cbt.soal_pg as soal,tb_detail_soal_cbt.pilihan_a as opsi_a,tb_detail_soal_cbt.pilihan_b as opsi_b,tb_detail_soal_cbt.pilihan_c as opsi_c, tb_detail_soal_cbt.pilihan_d as opsi_d,tb_detail_soal_cbt.pilihan_e as opsi_e, $pc_urut_soal1 AS jawaban FROM tb_detail_soal_cbt WHERE id_detail_soal_cbt = '".$pc_urut_soal[0]."'")->row();
						$soal_urut_ok[] = $ambil_soal; 
					}

					$a['detil_soal'] = $this->db->query("SELECT tb_soal_cbt.*,tb_guru.nama_guru as namaGuru, tb_pelajaran.nama_pelajaran as namaMapel, count(tb_detail_soal_cbt.id_soal_cbt) as jumlah_soal 
						,tb_soal_cbt.judul_soal as nama_ujian, tb_pelajaran.id_pelajaran as id_mapel FROM tb_mapping_cbt
										 JOIN tb_guru_mapel ON tb_mapping_cbt.id_guru_mapel = tb_guru_mapel.id_guru_mapel
										 JOIN tb_master_kelas ON tb_master_kelas.id_master_kelas = tb_guru_mapel.id_master_kelas
										 JOIN tb_guru ON tb_guru.id_guru = tb_guru_mapel.id_guru
										 JOIN tb_pelajaran ON tb_pelajaran.id_pelajaran = tb_guru_mapel.id_pelajaran
										 JOIN tb_soal_cbt ON tb_soal_cbt.id_soal_cbt = tb_mapping_cbt.id_soal_cbt
										 join tb_detail_soal_cbt on tb_detail_soal_cbt.id_soal_cbt = tb_soal_cbt.id_soal_cbt
										 JOIN tb_kelas_siswa on tb_kelas_siswa.id_master_kelas = tb_master_kelas.id_master_kelas
										  WHERE tb_mapping_cbt.id_mapping_cbt = '$uri4'")->row();
					//$soal_urut_ok = $this->db->query("SELECT * FROM m_soal ORDER BY RAND()")->result();
					$a['detiltes'] = $q_ambil_soal;
					$a['data'] = $soal_urut_ok;
				}
			} else {
				redirect('adm/sudah_selesai_ujian/'.$uri4);
			}
			//echo var_dump($a); 
			$this->load->view('aaa_ikut_ujian', $a);
		}
	}

	public function jvs() {
		$this->cek_aktif();
		
		$data_soal 		= $this->db->query("SELECT id, gambar, soal, opsi_a, opsi_b, opsi_c, opsi_d, opsi_e FROM m_soal ORDER BY RAND()")->result();
		
		$this->j($data_soal);
		exit;
	}

	// public function rubah_password() {
	// 	$this->cek_aktif();
		
	// 	//var def session
	// 	$a['sess_admin_id'] = $this->session->userdata('admin_id');
	// 	$a['sess_level'] = $this->session->userdata('admin_level');
	// 	$a['sess_user'] = $this->session->userdata('admin_user');
	// 	$a['sess_konid'] = $this->session->userdata('admin_konid');

	// 	//var def uri segment
	// 	$uri2 = $this->uri->segment(2);
	// 	$uri3 = $this->uri->segment(3);
	// 	$uri4 = $this->uri->segment(4);

	// 	//var post from json
	// 	$p = json_decode(file_get_contents('php://input'));
	// 	$ret = array();
	// 	if ($uri3 == "simpan") {
	// 		$p1_md5 = md5($p->p1);
	// 		$p2_md5 = md5($p->p2);
	// 		$p3_md5 = md5($p->p3);

	// 		$cek_pass_lama = $this->db->query("SELECT password FROM m_admin WHERE id = '".$a['sess_admin_id']."'")->row();

	// 		if ($cek_pass_lama->password != $p1_md5) {
	// 			$ret['status'] = "error";
	// 			$ret['msg'] = "Password lama tidak sama...";
	// 		} else if ($p2_md5 != $p3_md5) {
	// 			$ret['status'] = "error";
	// 			$ret['msg'] = "Password baru konfirmasinya tidak sama...";
	// 		} else if (strlen($p->p2) < 6) {
	// 			$ret['status'] = "error";
	// 			$ret['msg'] = "Password baru minimal terdiri dari 6 huruf..";
 // 			} else {
	// 			$this->db->query("UPDATE m_admin SET password = '".$p3_md5."' WHERE id = '".$a['sess_admin_id']."'");
	// 			$ret['status'] = "ok";
	// 			$ret['msg'] = "Password berhasil diubah...";
	// 		}
	// 		$this->j($ret);
	// 		exit;
	// 	} else {
	// 		$data = $this->db->query("SELECT id, kon_id, level, username FROM m_admin WHERE id = '".$a['sess_admin_id']."'")->row();
	// 		$this->j($data);
	// 		exit;
	// 	}
	// }

	public function sudah_selesai_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);

		
		$q_nilai = $this->db->query("SELECT nilai, tgl_selesai,tgl_mulai FROM tr_ikut_ujian WHERE id_tes = $uri3 AND id_user = '".$a['sess_konid']."' AND status = 'N'")->row();
		if (empty($q_nilai)) {
			redirect('adm/ikut_ujian/_/'.$uri3);
		} else {
			$a['p'] = "v_selesai_ujian";
		// $a['data'] = "<div class='alert alert-danger'>Anda telah selesai mengikuti ujian ini pada : <strong style='font-size: 16px'>".tjs($q_nilai->tgl_selesai, "l")."</strong> , Terimakasih</strong></div>";
			
	$dteStart = new DateTime($q_nilai->tgl_mulai);
   	$dteEnd   = new DateTime($q_nilai->tgl_selesai);
  	$dteDiff  = $dteStart->diff($dteEnd);
	$waktupengerjaan =  $dteDiff->format("%H:%I:%S");

			$a['data'] = "<div class='alert alert-danger'>Anda mengerjakan soal selama : <strong style='font-size: 16px'>".$waktupengerjaan."</strong></strong></div>";}

		$this->load->view('aaa', $a);
	}


	/* Login Logout */

	public function login() {
		$this->load->view('aaa_login');
	}
	
	public function act_login() {
		
		$username	= $_POST['username'];
		$password	= $_POST['password'];
		
		$password2	= md5($password);
		
		$q_data		= $this->db->query("SELECT * FROM mst_users WHERE username = '".$username."' AND password = '$password2'");
		$j_data		= $q_data->num_rows();
		$a_data		= $q_data->row();

		
		$_log		= array();
		if ($j_data === 1) {

			$sess_nama_user = "";

			if ($a_data->level == "SISWA") {
				$det_user = $this->db->query("SELECT nama_siswa FROM tb_siswa WHERE id_siswa = '".$a_data->kon_id."'")->row();
				if (!empty($det_user)) {
					$sess_nama_user = $det_user->nama_siswa;
				}
			} else if ($a_data->level == "GURU") {
				$det_user = $this->db->query("SELECT nama_guru FROM tb_guru WHERE id_guru = '".$a_data->kon_id."'")->row();
				if (!empty($det_user)) {
					$sess_nama_user = $det_user->nama_guru;
				}
			} else {
				$sess_nama_user = "Administrator Pusat";
			}

			$data = array(
                    'admin_id' => $a_data->id_users,
                    'admin_user' => $a_data->username,
                    'admin_level' => $a_data->level,
                    'admin_konid' => $a_data->kon_id,
                    'admin_nama' => $sess_nama_user,
					'admin_valid' => true
                    );
            $this->session->set_userdata($data);
			$_log['log']['status']			= "1";
			$_log['log']['keterangan']		= "Login berhasil";
			$_log['log']['detil_admin']		= $this->session->userdata;
		} else {
			$_log['log']['status']			= "0";
			$_log['log']['keterangan']		= "Maaf, username dan password tidak ditemukan";
			$_log['log']['detil_admin']		= null;
		}
		
		$this->j($_log);
	}
	
	public function logout() {
		$data = array(
                    'admin_id' 		=> "",
                    'admin_user' 	=> "",
                    'admin_level' 	=> "",
                    'admin_konid' 	=> "",
                    'admin_nama' 	=> "",
					'admin_valid' 	=> false
                    );
        $this->session->set_userdata($data);
		redirect('adm');
	}


	//fungsi tambahan
	public function get_akhir($tabel, $field, $kode_awal, $pad) {
		$get_akhir	= $this->db->query("SELECT MAX($field) AS max FROM $tabel LIMIT 1")->row();
		$data		= (intval($get_akhir->max)) + 1;
		$last		= $kode_awal.str_pad($data, $pad, '0', STR_PAD_LEFT);
	
		return $last;
	}

	
	public function bersih($teks) {
		return mysqli_real_escape_string($teks);
	}
	
	public function j($data) {
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
